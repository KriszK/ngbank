import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionContainerComponent } from './containers/transaction-container/transaction-container.component';
import { transactionRouting } from './transaction-routing.module';
import { TransactionTableComponent } from './components/transaction-table/transaction-table.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  imports: [
    CommonModule,
    transactionRouting,
    MaterialModule
  ],
  declarations: [TransactionContainerComponent, TransactionTableComponent]
})
export class TransactionModule { }
