export interface TransactionTableRow {
  date: string;
  account: string;
  amount: number;
  currency: string;
}
