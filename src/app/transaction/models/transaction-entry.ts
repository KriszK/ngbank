export declare class TransactionEntry {
    /**
     * source account of transaction - it\'s part of primary key
     */
    account: string;
    /**
     * date of event in yyyyMMddHHmmss - it\'s part of primary key
     */
    eventDate: string;
    /**
     * is it a card transaction or not. details:  transaction_type = B → true, or transaction_code is in    \"transactionCodesForGwbCardTransactions\" configuration list (in case of GWB transaction (transaction_type = \'G\')) or it is in    \"transactionCodesForUrbisCardTransactions\" configuration list (in case of GWB transaction (transaction_type = \'U\'))
     */
    isCardTransaction: boolean;
    /**
     * is this transaction virtual terminal transaction (card not present) - it can be occurred only in case of isCardTransaction = true  it will be true in case of the followings:      - POSID (terminal_id in merged_transaction table) begin with \"22\" (OTP)     - ECI indicator  ( e_comm field in merged_transaction table is true )
     */
    isVPOS: boolean;
    /**
     * is it a POS transaction   transaction_origin == POS or  isCardTransaction field is POS
     */
    isPOS?: boolean;
    transactionCode?: string;
    /**
     * This transaction is a credit transaction (income) credit_debit_indicator == \'CRDT\'
     */
    isCredit: boolean;
    /**
     * This transaction is booked or not. booking_date is not null and not empty
     */
    isBooked: boolean;
    /**
     * Date and time of booking of individually queried record in yyyy-MM-ddTHH:mm:ssZ format for example: 2017-07-21T00:00:00Z see  RFC 3339, section 5.6 usually this field will contain only date
     */
    bookingDate?: string;
    /**
     * Amount  of transaction  In case of card transaction: - event_amount in case of booked card transaction (isBooked true, isTransaction is true) - transaction_amount in case of not booked card transaction with HUF currency  (isBooked true, isTransaction is true, currency is HUF) - original_amount  in case of not booked card transaction with not HUF currency (isBooked  true, isTransaction is true, currency is not HUF)  In case of non card  transaction:  - event_amount in case of booked atm transaction (isBooked  true, isTransaction is true) transaction_amount in case of not booked  card transaction with HUF currency  - other_amount in case of not booked  atm  transaction (isBooked true, isTransaction is true) transaction_amount  in case of not booked atm transaction with not-HUF currency
     */
    amount: number;
    /**
     * effective currency of transaction in case of card transaction : transaction_currency by default, if original_currency is  null.  If original_currency is  not null then transaction usually is a transaction with foreign currency.   in case of atm transaction : event_currency by default, if other_currency is  null.  If other_currency is  not null then transaction usually is a transaction with foreign currency.
     */
    currency: string;
    /**
     * The category is the result of  manual categorization step.  manual_category  is not null.
     */
    isManualCategory: boolean;
    category: string;
    automaticCategory: string;
    /**
     * card number of transaction (only filled in case of card transaction )
     */
    cardNumber?: string;
    /**
     * \"reference_structured\" + \"reference_unstructured_1\" + \"reference_unstructured_2\"  null value should be trimmed.
     */
    narration: string;
    /**
     * Partner name of transaction.  terminal_addressline2 in case of card transaction  in case of atm transaction: initiator_name (in case of credit transaction), recipient_name (in case of debit transaction)  booking_narrative otherwise
     */
    partnerName: string;
    /**
     * Comma separated values of the following fields: \"terminal_town\", \"terminal_addressline1\", \"terminal_country\"
     */
    partnerAddress: string;
    /**
     * value of recipient_account in case of atm or transfer transactions (only in case of debit transaction)
     */
    targetAccount: string;
    /**
     * source account number in case of credit (income) transaction. - value of account in case of atm/transfer transaction - value of initiator_account in case of card transaction
     */
    sourceAccount: string;
    status: string;
    /**
     * partnerAddress field contain only town because transaction record in database contain partial fields.
     */
    isPartialPartnerAddress: boolean;
    /**
     * Need to show map on client side. It may depend on settings and vpos flag as well. It help client to make decision.
     */
    isShowMap: boolean;
    /**
     * Contain daily_no field  of database for client which need this information (requested by developers of Supercharge)
     */
    dailyNo: string;
    /**
     * Contain original_currency field  of database for client which need this information (requested by developers of Supercharge)
     */
    originalCurrency: string;
    /**
     * Contain original_amount field  of database for client which need this information (requested by developers of Supercharge)
     */
    originalAmount: number;
    /**
     * sourceSystem is part of database key - it will be necessary to set category (requested by developers of Supercharge)
     */
    sourceSystem: string;
    /**
     * p8Token is part of database key - it will be necessary to set category (requested by developers of Supercharge)
     */
    p8Token: string;
    /**
     * Optional booking_narrative field of merged_transaction table. It was used in DSP 1.0 as transaction type. It can be used in  case of booked transaction. subType in case of card transaction.
     */
    bookingNarrative?: string;
}
