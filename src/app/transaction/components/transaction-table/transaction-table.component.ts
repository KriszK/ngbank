import { Component, OnInit, Input } from '@angular/core';
import { TransactionTableRow } from './../../models/transaction-table-row';

@Component({
  selector: 'app-transaction-table',
  templateUrl: './transaction-table.component.html',
  styleUrls: ['./transaction-table.component.css']
})
export class TransactionTableComponent implements OnInit {

  @Input() data: TransactionTableRow[];

  displayedColumns: string[] = ['date', 'account', 'amount'];

  constructor() { }

  ngOnInit() {
  }

}
