import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { getTransactions } from '../mock/transaction-response-mock';
import { TransactionEntry } from '../models/transaction-entry';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor() { }

  getTransactions$(): Observable<TransactionEntry[]> {
    return of(getTransactions()).pipe(delay(3000));
  }
}
