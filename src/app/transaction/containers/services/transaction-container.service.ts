import { TransactionService } from './../../services/transaction.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TransactionTableRow } from '../../models/transaction-table-row';
import { map } from 'rxjs/operators';
import { TransactionEntry } from '../../models/transaction-entry';

@Injectable({
  providedIn: 'root'
})
export class TransactionContainerService {

  constructor(private transactionService: TransactionService) { }

  mapTransactions$(): Observable<TransactionTableRow[]> {
    return this.transactionService.getTransactions$().pipe(
      map((response: TransactionEntry[]) => {
        return response.map((data: TransactionEntry) => {
          return {
            date: data.eventDate,
            account: data.account,
            amount: data.amount,
            currency: data.currency
          }
        });
      })
    );

  }
}
