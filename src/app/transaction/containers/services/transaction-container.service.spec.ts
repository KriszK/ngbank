import { TestBed, inject } from '@angular/core/testing';

import { TransactionContainerService } from './transaction-container.service';

describe('TransactionContainerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransactionContainerService]
    });
  });

  it('should be created', inject([TransactionContainerService], (service: TransactionContainerService) => {
    expect(service).toBeTruthy();
  }));
});
