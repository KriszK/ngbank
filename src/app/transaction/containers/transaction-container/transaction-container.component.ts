import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { TransactionTableRow } from '../../models/transaction-table-row';
import { TransactionContainerService } from '../services/transaction-container.service';

@Component({
  selector: 'app-transaction-container',
  templateUrl: './transaction-container.component.html',
  styleUrls: ['./transaction-container.component.css'],
  providers: [TransactionContainerService]
})
export class TransactionContainerComponent {
  transactionVM$: Observable<TransactionTableRow[]>;

  constructor(private transactionService: TransactionContainerService) {
    this.transactionVM$ = this.transactionService.mapTransactions$();
   }

}
