import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionContainerComponent } from './containers/transaction-container/transaction-container.component';

export const routes: Routes = [
  { path: '', component: TransactionContainerComponent }
];

export const transactionRouting: ModuleWithProviders = RouterModule.forChild(routes);
